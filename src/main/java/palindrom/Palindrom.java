package palindrom;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Czy słowo które wpiszesz jest palindromem?");
        String wordToCheck = sc.nextLine();


        boolean isPalindrom = true;
        int start = 0;
        int end = wordToCheck.length() - 1;

        while (start <= end) {
            System.out.println("Cheking equality against ->> " + wordToCheck.charAt(start) + " ? " + wordToCheck.charAt(end));
            if (wordToCheck.charAt(start) != wordToCheck.charAt(end)) {
                isPalindrom = false;
                break;
            }

            start++;
            end--;
        }
       if (isPalindrom) {
           System.out.println("\nSłowo: " + wordToCheck + " jest palindromem");
       } else {
           System.out.println("\nSłowo: " + wordToCheck + " nie jest palindromem!!!");
       }
    }
}

