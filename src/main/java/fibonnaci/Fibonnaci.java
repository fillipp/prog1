package fibonnaci;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Scanner;

public class Fibonnaci {
    // ciąg fibonaciego gdzie następna liczba jest sumą dwuch poprzednich
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wartość n elementu ciągu Fibonnaciego:");
        System.out.print("n = ");

        int n = scanner.nextInt();
        LocalTime countStart = LocalTime.now();

        System.out.println("n-ty element ciągu to: " + fibonnaci(n));

        LocalTime countEnd = LocalTime.now();
        Duration courseTime = Duration.between(countEnd, countStart);
        System.out.println(courseTime.getSeconds());

    }

    static long fibonnaci(long n) {
        if (n >= 3) {
            return fibonnaci(n -1) + fibonnaci(n -2 );
        } else {
            return 1;
        }
    }
}
