package lock;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj kombinacje do zamknięcia zamka w formacie X-X-X");
        Scanner scanner = new Scanner(System.in);
        String[] lockCombination = scanner.nextLine().split("-");
//        String[] lockSplitted = lockCombination.split("-");


        Lock lock = new Lock(
                Integer.parseInt(lockCombination[0]),
                Integer.parseInt(lockCombination[1]),
                Integer.parseInt(lockCombination[2]));

        System.out.println("Tworze zamek lock --> " + lock);
        lock.shuffle();
        lock.switchA();
        lock.switchC();
        System.out.println(lock);

        if (lock.isOpen()) {
            System.out.println("Zamek jest otwarty");
        } else {
            System.out.println("Zamek jest zamkniety");
        }

    }
}
