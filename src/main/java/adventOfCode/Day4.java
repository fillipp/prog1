package adventOfCode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day4 {

    public static void main(String[] args) {
        List<String> integerList = new ArrayList<>();

        for (int i = 136818; i <= 685979; i++) {
            integerList.add(Integer.toString(i));
        }

        System.out.println(integerList.stream()
                .filter(integer -> isMatch(integer)).collect(Collectors.toList()).size());

    }

    private static boolean isMatch(String numberString) {

        int count = 0;

        for (int i = 0; i < 5; i++) {
            if (numberString.charAt(i) > numberString.charAt(i + 1)) {
                return false;
            }
        }


        for (int i = 0; i < 5; i++) {

            char a = numberString.charAt(i);
            char b = numberString.charAt(i + 1);


            if (a == b) {
                count++;

            } else {

                if (count == 1) {

                    return true;
                }

                count = 0;
            }

            if (i == 4 && count == 1) {

                return true;
            }

        }

        return false;
    }

}