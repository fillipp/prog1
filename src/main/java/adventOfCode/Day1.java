package adventOfCode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {
    public static void main(String[] args) throws IOException {
        //Paliwo potrzebne do uruchomienia danego modułu zależy od jego masy.
        // W szczególności, aby znaleźć paliwo wymagane dla modułu, należy wziąć jego masę,
        // podzielić przez trzy, zaokrąglić w dół i odjąć 2.


        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day1input"));

        List<String> listOfModuleMass = inputValues.collect(Collectors.toList());

        System.out.println(listOfModuleMass);

        int allRequiredFuel = 0;
        for (String mass: listOfModuleMass) {
            int requiredFuell = calculateFuel(Integer.parseInt(mass));
            System.out.println("Fuel needed for mass " + mass + "->>>" + requiredFuell);
            allRequiredFuel = allRequiredFuel + requiredFuell;
        }
        System.out.println("All required fuel: " + allRequiredFuel);

    }

    public static int calculateFuel(int mass){
        int fuel = (mass/3) - 2;
        if (fuel <= 0) {
            return 0;
        } else {
            return fuel + calculateFuel((fuel));
        }
    }
}
