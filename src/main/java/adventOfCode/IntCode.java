package adventOfCode;

public class IntCode {

    private int pointerOut;
    private int pointerCode;
    private byte modA;
    private byte modB;
    private byte modC;
    private byte command;
    private int valueA;
    private int valueB;
    private int valueC;
    private String[] inputPuzzle;
    private int input;
    private int phase;
    private boolean phaseStatus = true;
    private int powerOut;

    public int test(int input, String[] inputPuzzle) {

        this.phase = input;
        return test(phase, input, inputPuzzle);
    }

    public int test(int phase, int input, String[] inputPuzzle) {

        this.phase = phase;
        this.input = input;
        this.inputPuzzle = inputPuzzle;

        do {
            deCode(this.inputPuzzle);

            if (command == 1) {
                sub();
            }

            if (command == 2) {
                mul();
            }

            if (command == 3) {
                input();
            }

            if (command == 4) {
                printCode();
            }

            if (command == 5) {
                jumpIfTrue();
            }

            if (command == 6) {
                jumpIfFalse();
            }

            if (command == 7) {
                lessThen();
            }

            if (command == 8) {
                equal();
            }

        }while (command != 4 && command != 9);

        return powerOut;
    }

    private void deCode(String[] puzzle) {

        String code = puzzle[pointerCode];

        for (int i = 5; i > code.length(); ) {
            code = "0" + code;
        }

        modC = Byte.parseByte(String.valueOf(code.charAt(0)));
        modB = Byte.parseByte(String.valueOf(code.charAt(1)));
        modA = Byte.parseByte(String.valueOf(code.charAt(2)));
        command = Byte.parseByte(String.valueOf(code.charAt(4)));

        if (command == 1 || command == 2 || command == 7 || command == 8) {
            valueA = Integer.parseInt(puzzle[pointerCode + 1]);
            valueB = Integer.parseInt(puzzle[pointerCode + 2]);
            valueC = Integer.parseInt(puzzle[pointerCode + 3]);
        }

        if (command == 3 || command == 4) {
            valueA = Integer.parseInt(puzzle[pointerCode + 1]);
        }

        if (command == 5 || command == 6) {
            valueA = Integer.parseInt(puzzle[pointerCode + 1]);
            valueB = Integer.parseInt(puzzle[pointerCode + 2]);
        }

        pointerCode = pointerCode + 1;
    }

    private void sub() {

        readValue(setPointer(modA, valueA), setPointer(modB, valueB), setPointer(modC, valueC));

        inputPuzzle[pointerOut] = String.valueOf(valueA + valueB);
    }

    private void mul() {

        readValue(setPointer(modA, valueA), setPointer(modB, valueB), setPointer(modC, valueC));

        this.inputPuzzle[pointerOut] = String.valueOf(valueA * valueB);
    }

    private void input() {

        int input = this.input;

        if (phaseStatus) {
            input = phase;
            phaseStatus = false;
        }

        inputPuzzle[setPointer(modA, valueA)] = String.valueOf(input);
    }

    private void printCode() {

        readValue(setPointer(modA, valueA));
        powerOut = valueA;
    }

    private void jumpIfTrue() {

        readValue(setPointer(modA, valueA), setPointer(modB, valueB));

        if (valueA != 0) {
            pointerCode = valueB;
        }
    }

    private void jumpIfFalse() {

        readValue(setPointer(modA, valueA), setPointer(modB, valueB));

        if (valueA == 0) {
            pointerCode = valueB;
        }
    }

    private void lessThen() {

        readValue(setPointer(modA, valueA), setPointer(modB, valueB), setPointer(modC, valueC));

        String out;

        if (valueA < valueB) {
            out = String.valueOf(1);

        } else {
            out = String.valueOf(0);
        }

        inputPuzzle[pointerOut] = out;
    }

    private void equal() {
        readValue(setPointer(modA, valueA), setPointer(modB, valueB), setPointer(modC, valueC));

        String out;

        if (valueA == valueB) {
            out = String.valueOf(1);

        } else {
            out = String.valueOf(0);
        }

        inputPuzzle[pointerOut] = out;
    }

    private void readValue(int pointerA) {

        valueA = Integer.parseInt(inputPuzzle[pointerA]);
    }

    private void readValue(int pointerA, int pointerB) {

        readValue(pointerA);

        valueB = Integer.parseInt(inputPuzzle[pointerB]);
    }

    private void readValue(int pointerA, int pointerB, int pointerC) {

        readValue(pointerA, pointerB);

        pointerOut = pointerC;
    }


    private int setPointer(byte mod, int value) {

        int pointer;

        if (mod == 0) {
            pointer = value;
        } else {
            pointer = pointerCode;
        }

        pointerCode = pointerCode + 1;

        return pointer;
    }

    public int getCommand() {
        return command;
    }
}



