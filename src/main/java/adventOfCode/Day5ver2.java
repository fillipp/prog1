package adventOfCode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Day5ver2 {

    public static void main(String[] args) throws IOException {

        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day5input"));

        String[] inputPuzzle = inputValues.map(e -> e.split(",")).collect(Collectors.toList()).get(0);


        IntCode intCode = new IntCode();
        System.out.println("\nDiagnostic code: ");
        System.out.println(intCode.test(5, inputPuzzle));
    }

}