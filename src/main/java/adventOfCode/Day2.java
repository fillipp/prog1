package adventOfCode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day2 {
    public static void main(String[] args) throws IOException {

        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day2input"));

        String[] numbersArray = inputValues.map(e -> e.split(",")).collect(Collectors.toList()).get(0);
        System.out.println("Start: " + Arrays.toString(numbersArray));

        int noun;
        int verb;
        int result = 0;

        for (int i = 0; i <= 99; i++) {
            for (int j = 0; j <= 99; j++) {
                if (intCode(i, j, numbersArray.clone()) == 19690720) {
                    verb = j;
                    noun = i;
                    result = 100 * noun + verb;
                }
            }

            if (result != 0) {
                break;
            }

        }


        System.out.println("If start code is 1202 " + intCode(12, 02, numbersArray.clone()));

        System.out.println("Result: " + result);

    }

    public static int intCode(int pointerA1, int pointerA2, String[] intArray) {
        intArray[1] = String.valueOf(pointerA1);
        intArray[2] = String.valueOf(pointerA2);

        for (int i = 0; i <= intArray.length; i = i + 4) {

            int out = Integer.parseInt(intArray[i + 3]);
            int a = Integer.parseInt(intArray[Integer.parseInt(intArray[i + 1])]);
            int b = Integer.parseInt(intArray[Integer.parseInt(intArray[i + 2])]);
            int counter = Integer.parseInt(intArray[i]);

            if (counter == 99) {
                System.out.println(Arrays.toString(intArray));
                break;
            }

            if (counter == 1) {
                intArray[out] = String.valueOf(a + b);
            }

            if (counter == 2) {
                intArray[out] = String.valueOf(a * b);
            }

        }

        return Integer.parseInt(intArray[0]);

    }
}

