package adventOfCode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Day5ver1 {

    public static void main(String[] args) throws IOException {

        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day5input"));

        String[] testCodeArray = inputValues.map(e -> e.split(",")).collect(Collectors.toList()).get(0);

        System.out.println(intCode("5", testCodeArray));

    }


    public static String intCode(String input, String[] testCode) {

        int pointer = 0;

        do {

            String code = testCode[pointer];
            int[] decode = getCode(code);

            int pointerA;

            if (decode[2] == 0) {
                pointerA = Integer.parseInt(testCode[pointer + 1]);
            } else {
                pointerA = pointer + 1;
            }

            int pointerB;

            if (decode[1] == 0) {
                pointerB = Integer.parseInt(testCode[pointer + 2]);
            } else {
                pointerB = pointer + 2;
            }

            int pointerOut;

            if (decode[0] == 0) {
                pointerOut = Integer.parseInt(testCode[pointer + 3]);
            } else {
                pointerOut = pointer + 3;
            }


            int valueA;
            int valueB;

            if (decode[4] == 1) {
                valueA = Integer.parseInt(testCode[pointerA]);
                valueB = Integer.parseInt(testCode[pointerB]);
                testCode[pointerOut] = String.valueOf(valueA + valueB);
                pointer = pointer + 4;
            }

            if (decode[4] == 2) {
                valueA = Integer.parseInt(testCode[pointerA]);
                valueB = Integer.parseInt(testCode[pointerB]);
                testCode[pointerOut] = String.valueOf(valueA * valueB);
                pointer = pointer + 4;
            }

            if (decode[4] == 3) {
                testCode[pointerOut] = input;
                pointer = pointer + 2;
            }

            if (decode[4] == 4) {
                System.out.println(testCode[pointerA]);
                pointer = pointer + 2;
            }

            if (decode[4] == 5) {
                valueA = Integer.parseInt(testCode[pointerA]);
                valueB = Integer.parseInt(testCode[pointerB]);

                if (valueA != 0) {
                    pointer = valueB;

                } else {
                    pointer = pointer + 3;
                }
            }

            if (decode[4] == 6) {
                valueA = Integer.parseInt(testCode[pointerA]);
                valueB = Integer.parseInt(testCode[pointerB]);

                if (valueA == 0) {
                    pointer = valueB;

                } else {
                    pointer = pointer + 3;
                }
            }

            if (decode[4] == 7) {
                valueA = Integer.parseInt(testCode[pointerA]);
                valueB = Integer.parseInt(testCode[pointerB]);

                if (valueA < valueB) {
                    testCode[pointerOut] = String.valueOf(1);

                } else {
                    testCode[pointerOut] = String.valueOf(0);
                }

                pointer = pointer + 4;
            }

            if (decode[4] == 8) {
                valueA = Integer.parseInt(testCode[pointerA]);
                valueB = Integer.parseInt(testCode[pointerB]);

                if (valueA == valueB) {
                    testCode[pointerOut] = String.valueOf(1);

                } else {
                    testCode[pointerOut] = String.valueOf(0);
                }

                pointer = pointer + 4;
            }


            if (decode[4] == 9) {
                break;
            }


        } while (pointer < testCode.length);

        return "end";
    }

    private static int[] getCode(String code) {
        int[] decodeCode = new int[5];

        for (int i = 0; i < code.length(); i++) {
            decodeCode[4 - i] = Integer.parseInt(String.valueOf(code.charAt(code.length() - 1 - i)));

        }

        return decodeCode;
    }


}
