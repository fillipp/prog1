package adventOfCode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day7 {

    public static void main(String[] args) throws IOException {
        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day7input"));

        String[] inputPuzzle = inputValues.map(e -> e.split(",")).collect(Collectors.toList()).get(0);

        // part 1
        System.out.println("\nPart 1: Max power :" + maxPower(new int[]{0, 1, 2, 3, 4}, false, inputPuzzle) + "\n");

//        System.out.println("max power: " + ampPowerWithFeedbackLoop(new int[]{9, 7, 8, 5, 6}, 0, inputPuzzle));

        System.out.println("\nPart 2: Max power :" + maxPower(new int[]{5, 6, 7, 8, 9}, true, inputPuzzle));

    }

    private static int ampPowerWithFeedbackLoop(int[] phases, String[] inputPuzzle) {

        int powerE = 0;
        IntCode ampA = new IntCode();
        IntCode ampB = new IntCode();
        IntCode ampC = new IntCode();
        IntCode ampD = new IntCode();
        IntCode ampE = new IntCode();

        do {
            int powerA = ampA.test(phases[0], powerE, inputPuzzle.clone());
            int powerB = ampB.test(phases[1], powerA, inputPuzzle.clone());
            int powerC = ampC.test(phases[2], powerB, inputPuzzle.clone());
            int powerD = ampD.test(phases[3], powerC, inputPuzzle.clone());
            powerE = ampE.test(phases[4], powerD, inputPuzzle.clone());
//            System.out.println("powerA: " + powerA + ", powerB: " + powerB + ", powerC: " + powerC + ", powerD: " + powerD + ", powerE: " + powerE);
        } while (ampE.getCommand() != 9);

        return powerE;
    }

    private static int nAmpPower(int[] phases, int nAmp, String[] inputPuzzle) {
        IntCode amp = new IntCode();

        if (nAmp > 1) {
            return amp.test(phases[nAmp - 1], nAmpPower(phases, nAmp - 1, inputPuzzle), inputPuzzle);
        }
        return amp.test(phases[0], 0, inputPuzzle);
    }


    private static int maxPower(int[] initPhases, boolean feedbackLoop, String[] inputPuzzle) {

        int maxPower;
        int maxPowerOld = 0;
        int[] phases = initPhases.clone();
        int[] temp;


        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                for (int k = 0; k < 5; k++) {
                    for (int l = 0; l < 5; l++) {
                        for (int m = 0; m < 5; m++) {
                            phases[0] = (i % 5) + initPhases[0];
                            phases[1] = (j % 5) + initPhases[0];
                            phases[2] = (k % 5) + initPhases[0];
                            phases[3] = (l % 5) + initPhases[0];
                            phases[4] = (m % 5) + initPhases[0];

                            temp = phases.clone();
                            Arrays.sort(temp);

                            if (Arrays.equals(temp, initPhases)) {

                                if (feedbackLoop == false) {
                                    maxPower = nAmpPower(phases, 5, inputPuzzle);
                                } else {
                                    maxPower = ampPowerWithFeedbackLoop(phases, inputPuzzle);
                                }

                                if (maxPower > maxPowerOld) {
                                    maxPowerOld = maxPower;
                                }
                                System.out.println("Fazy:" + Arrays.toString(phases) + ": " + maxPower);
                            }
                        }
                    }
                }
            }
        }
        return maxPowerOld;
    }

}
