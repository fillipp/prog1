package adventOfCode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day6 {

    public static void main(String[] args) throws IOException {
        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day6input"));

        List<String[]> collect = inputValues.map(e -> e.split("\\)")).collect(Collectors.toList());

        List<List<String>> pathToCom = new ArrayList<>();

        System.out.println("\nSuma wszystkich orbit: " + counterAllOrbits(collect));

        System.out.println("\nLista transferów od " + pathToComList(collect, pathToCom, "YOU") + " do COM:\n" + pathToCom.get(pathToCom.size()-1));

        System.out.println("\nLista transferów od " + pathToComList(collect, pathToCom, "SAN") + " do COM:\n" + pathToCom.get(pathToCom.size()-1));

        System.out.println("\nNajmniejsza liczba transferów do Santy: " + numberOfTransfersBetween2Planets("YOU", "SAN", collect, pathToCom));

    }

    private static int numberOfTransfersBetween2Planets(String from, String to, List<String[]> collect, List<List<String>> pathToCom) {
        int indexFromList = 0;
        int indexToList = 0;
        int transfersFrom = 0;
        int transfersTo = 0;

        for (int i = 0; i < pathToCom.size() ; i++) {

            if (pathToCom.get(i).get(0).equals(from)) {
                indexFromList = i;
            }

            if (pathToCom.get(i).get(0).equals(to)) {
                indexToList = i;
            }
        }

        for (int i = 0; i < pathToCom.get(indexFromList).size(); i++) {

            String planetFrom = pathToCom.get(indexFromList).get(i);

            if (pathToCom.get(indexToList).stream().anyMatch(s -> s.equals(planetFrom))) {
                transfersFrom = i - 1;
                break;
            }
        }
        for (int i = 0; i < pathToCom.get(indexToList).size(); i++) {

            String planetFrom = pathToCom.get(indexToList).get(i);

            if (pathToCom.get(indexFromList).stream().anyMatch(s -> s.equals(planetFrom))) {
                transfersTo = i - 1;
                break;
            }
        }
        return transfersFrom + transfersTo;
    }


    private static int counterAllOrbits(List<String[]> collect) {

        String temp;
        int counterOrbits = 0;

        for (int i = 0; i < collect.size(); i++) {

            temp = collect.get(i)[1];

            for (int j = 0; j < collect.size(); j++) {

                if (collect.get(j)[1].equals(temp)) {
                    counterOrbits = counterOrbits + 1;
                    temp = collect.get(j)[0];
                    j = -1;
                }
            }
        }

        return counterOrbits;
    }


    private static String pathToComList(List<String[]> collect, List<List<String>> pathToCom, String planet) {

        List<String> tempList = new ArrayList<>();
        tempList.add(planet);
        pathToCom.add(tempList);

        for (int j = 0; j < collect.size(); j++) {

            if (collect.get(j)[1].equals(tempList.get(tempList.size() - 1))) {

                tempList.add(collect.get(j)[0]);
                j = -1;
            }
        }

        return tempList.get(0);
    }
}
