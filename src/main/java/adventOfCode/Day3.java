package adventOfCode;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day3 {

    public static int x, y = 0;

    public static void main(String[] args) throws IOException {
        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day3input"));


        List<String[]> wires = inputValues.map(e -> e.split(",")).collect(Collectors.toList());

        String[] firstWire = wires.get(0);
        String[] secondWire = wires.get(1);

        List<Point> firstPointList = new ArrayList<>();
        List<Point> secondPointList = new ArrayList<>();
        List<Point> crossList = new ArrayList<>();
        List<Integer> countStepList = new ArrayList<>();


        Arrays.stream(firstWire)
                .map(e -> firstPointList.addAll(mapToObject(e)))
                .collect(Collectors.toList());

        x = y = 0;

        Arrays.stream(secondWire)
                .map(e -> secondPointList.addAll(mapToObject(e)))
                .collect(Collectors.toList());


        for (int i = 0; i < firstPointList.size(); i++) {
            for (int j = 0; j < secondPointList.size(); j++) {
                if (firstPointList.get(i).x == secondPointList.get(j).x && firstPointList.get(i).y == secondPointList.get(j).y) {
                    crossList.add(firstPointList.get(i));
                    countStepList.add((i + 1) + (j + 1));
                }
            }
        }

        Optional<Integer> minLengthToCross = crossList.stream()
                .map(e -> (Math.abs(e.x) + Math.abs(e.y)))
                .min(Integer::compareTo);

        System.out.println("Najbliższy węzeł znajduje się w odległości: " + minLengthToCross.get());

        Optional<Integer> minStep = countStepList.stream().min(Integer::compareTo);

        System.out.println("Najkrótsza droga do węzła znajduje się w odległości: " + minStep.get());
    }

    public static List<Point> mapToObject(String commandArray) {
        char direction = commandArray.charAt(0);
        int move = Integer.parseInt(commandArray.substring(1));

        List<Point> list = new ArrayList<>();

        if (direction == 'L') {
            for (int i = 0; i < move; i++) {
                Point point = new Point(x, y);
                x = x - 1;
                point.setX(x);
                list.add(i, point);
            }
        }

        if (direction == 'R') {
            for (int i = 0; i < move; i++) {
                Point point = new Point(x, y);
                x = x + 1;
                point.setX(x);
                list.add(point);
            }
        }

        if (direction == 'U') {
            for (int i = 0; i < move; i++) {
                Point point = new Point(x, y);
                y = y + 1;
                point.setY(y);
                list.add(point);
            }
        }

        if (direction == 'D') {
            for (int i = 0; i < move; i++) {
                Point point = new Point(x, y);
                y = y - 1;
                point.setY(y);
                list.add(point);
        }
        }
        return list;
    }
}
