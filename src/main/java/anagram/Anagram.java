package anagram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {
        System.out.println("Podaj dwa słowa do sprawdzenia czy są anagramami");
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj piwrwsze słowo : ");
        String firstWord = scanner.next();
        System.out.println("Podaj drugie słowo: ");
        String secondWord = scanner.next();

        char[] firstWordCharacters = firstWord.toCharArray();
        char[] secondWordCharacters = secondWord.toCharArray();
        Arrays.sort(firstWordCharacters);
        Arrays.sort(secondWordCharacters);

        boolean isAnagram = Arrays.equals(firstWordCharacters, secondWordCharacters);

        if (isAnagram) {
            System.out.println("Podane słowa są anagramem");
        } else {
            System.out.println("Podane słowo nie jest anagramem");
        }


    }
}
