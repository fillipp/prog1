package calculator;

import java.util.function.IntBinaryOperator;

public enum Example {
    RED("CZE") {
        @Override
        public void something() {
            System.out.println("Something RED");
        }
    },
    GREEN("ZIE") {
        @Override
        public void something() {
            System.out.println("Something GREEN");
        }
    },
    BLUE("NIE") {
        @Override
        public void something() {
            System.out.println("Something BLUE");
        }
    };

    public abstract void something();

    private String desc;


    Example(String description) {

        this.desc = description;
    }

   public String getDesc() {
        return desc;
    }
}


