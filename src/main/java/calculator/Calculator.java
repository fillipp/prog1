package calculator;

import java.util.function.IntBinaryOperator;

public enum Calculator {
    ADD("dodawanie", (x, y) -> x + y),

    MULTIPLY("odejmowanie", new IntBinaryOperator() {
        @Override
        public int applyAsInt(int left, int right) {
            return left * right;
        }
    }
    ),

    SUBTRACT("mnozenie", (x, y) -> x - y);


    public int calculate(int a, int b) {
        return operator.applyAsInt(a, b);
    }

    private String descryption;
    private IntBinaryOperator operator;

    Calculator(String descryption, IntBinaryOperator operator) {
        this.descryption = descryption;
        this.operator = operator;
    }

}
