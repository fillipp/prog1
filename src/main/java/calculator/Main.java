package calculator;

public class Main {
    public static void main(String[] args) {
        Example.RED.something();
        Example.GREEN.something();
        Example.BLUE.something();

        System.out.println(Example.RED.getDesc());
        System.out.println(Example.GREEN.getDesc());
        System.out.println(Example.BLUE.getDesc());

        System.out.println(Calculator.ADD.calculate(2, 2));
        System.out.println(Calculator.MULTIPLY.calculate(2, 3));
        System.out.println(Calculator.SUBTRACT.calculate(2, 1));
    }
}
