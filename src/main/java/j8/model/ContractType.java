package j8.model;

public enum ContractType {
    F("Full"),
    H("Half");

    String description;

    ContractType(String desc) {
        this.description =desc;
    }

    public  String getDescription() {
        return description;
    }
}
