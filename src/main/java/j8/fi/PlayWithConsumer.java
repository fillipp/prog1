package j8.fi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PlayWithConsumer {
    public static void main(String[] args) {


//    Consumer<String> consumer =
//    Consumer<String> consumer = (T t)
//    Consumer<String> consumer = (String t)
//    Consumer<String> consumer = (String t) -> System.out.println(t)

        Consumer<String> consumer = System.out::println;
//   lub
//      Consumer<String> consumer = (String t) -> System.out.println(t);

        consumer.accept("jakiś String");

        List<String> stringList = new ArrayList<>(Arrays.asList("Basia","Kasia", "Ania"));

        Consumer<List<String>> listConsumer = List::clear;
//      lub
//      Consumer<List<String>> listConsumer = (List<String> t) -> t.clear();

        System.out.println(stringList.size());
        listConsumer.accept(stringList);
        System.out.println(stringList.size());

        MyConsumer<List<String>> addConsumer = (List<String> t) -> t.add("test");
        MyConsumer<List<String>> secondConsumer = (List<String> t) -> t.add("Hello");

        MyConsumer<List<String>> groupingConsumer = addConsumer.andThen(secondConsumer);

        groupingConsumer.accept(stringList);

        System.out.println(stringList);


    }
}