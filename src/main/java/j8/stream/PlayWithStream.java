package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PlayWithStream {
    public static void main(String[] args) {

        List<Employee> employees = FileUtils.load(Path.of("src/main/resources/j8/employees.csv"));

//        zad1(employees);
//        zad2(employees);
//        zad3(employees);
//        zad4(employees);
//        zad5(employees);
//        zad6(employees);
//        zad7(employees);
//        zad8(employees);
//        zad9(employees);
//        zad10(employees);
//        zad11(employees);
//        zad12(employees);
//        zad13(employees);
//        zad14(employees);
//        zad14a(employees);
//        zad14c(employees);
//        zad14d(employees);
        zad15(employees);
        intStatic(employees);


    }

    private static void intStatic(List<Employee> employees) {
        //IntStatistic
        System.out.println(employees.stream()
        .mapToDouble(e -> e.getSalary()));
//        .summaryStatistics;
    }

    private static void zad15(List<Employee> employees) {
        Optional<Employee> collect = employees.stream()
                .collect(
                        Collectors.maxBy(
                                Comparator.comparing(Employee::getSalary)
                        )
                );

        Optional<Employee> empGoo = employees.stream()
                .filter(e -> e.getFirstName().equals("GOOOO"))
                .findFirst();

        System.out.println(empGoo); //optional jest pusty
        empGoo.ifPresentOrElse(     //rozwiazanie za pomoca codowania funkcyjnego
                e -> System.out.println(e.getFirstName()),
                () -> System.out.println("Tego gościa nie ma")
        );
        System.out.println(collect);

        //IntStatistic
        System.out.println(employees.stream()
                .mapToDouble(e -> e.getSalary())
                .summaryStatistics());

        System.out.println(IntStream.of(1, 2, 4, 5, 6)
                .summaryStatistics());

    }

    private static void zad14d(List<Employee> employees) {
        Map<String, Integer> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                e -> e.getFirstName(),
                                Collectors.collectingAndThen(           //MAP<String, List<>>
                                        Collectors.counting(),          // LONG
                                        value -> value.intValue()       //LONG >>> INT
                                )
                        )
                );
        System.out.println(collect);
    }

    private static void zad14c(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .filter(e -> e.getFirstName().endsWith("d"))
                .collect(
                        Collectors.groupingBy(
                                e -> e.getFirstName(),
                                Collectors.counting()

                        )
                );
        System.out.println(collect);

    }

    private static void zad14a(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                e -> e.getFirstName(),
                                Collectors.counting()
                        )
                );
        System.out.println(collect);
    }

    private static void zad14(List<Employee> employees) {
        Map<String, List<Employee>> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                e -> e.getFirstName()
                        )
                );
        System.out.println(collect);

    }

    private static void zad13(List<Employee> employees) {
        Map<String, Double> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                e -> e.getType().name(),
                                Collectors.averagingDouble(
                                        e -> e.getSalary()
                                )
                        )
                );

        System.out.println(collect);


    }

    private static void zad12(List<Employee> employees) {
        Map<Boolean, Long> collect = employees.stream()
                .collect(Collectors.partitioningBy(
                        e -> e.getSalary() > 5000,  //filtruje tych co zarabiają więcej niz 5000
                        Collectors.counting()       //zlicza ile employow spelnia a ile nie spelnia warunku
                        )
                );
        System.out.println(collect);
    }

    private static void zad11(List<Employee> employees) {
        String collect = employees.stream()
                .map(Employee::getLastName)
                .collect(Collectors.joining(","));
        System.out.println(collect);

    }

    private static void zad10(List<Employee> employees) {
        employees.stream()
                .sorted(
                        (person1, person2) -> {
                            int lastNameComparator =
                                    person1.getLastName()
                                            .compareTo(person2.getLastName());

                            if (lastNameComparator != 0) {
                                return lastNameComparator;
                            }

                            return person1.getLastName()
                                    .compareTo(person2.getFirstName());
                        }


                ).forEach(System.out::println);

//  lub krótszy zapis
        employees.stream()
                .sorted(
                        Comparator.comparing(Employee::getLastName)
                                .thenComparing(Employee::getFirstName)
                ).forEach(System.out::println);
    }

    private static void zad9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getFirstName().charAt(1) == 'a')
                .filter(e -> e.getLastName().charAt(3) == 'e')
                .forEach(System.out::println);
    }

    private static void zad8(List<Employee> employees) {
        Map<String, List<Double>> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                Employee::getLastName,
                                Collectors.mapping(
                                        e -> e.getSalary() * 1.12, Collectors.toList()
                                )
                        )
                );
        System.out.println(collect);
    }

    private static void zad7(List<Employee> employees) {
        System.out.println(employees.stream()
                .anyMatch(e -> e.getLastName().equals("Kowalska")));
    }

    private static void zad6(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getProfession().toUpperCase())
                .forEach(System.out::println);
    }

    private static void zad5(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getLastName())
                .map(e -> e.substring(e.length() - 3))
                .map(e ->
                        (e.endsWith("ski") || e.endsWith("ska"))
                                ? e.toUpperCase() : e   //jezeli wynikiem warunku logicznego jest true to zamienia literki jezeli nie zostawia bez zmian
                )
                .forEach(System.out::println);
    }

    private static void zad4(List<Employee> employees) {
        employees.stream()
                .map(Employee::getFirstName)
                .forEach(System.out::println);
    }

    private static void zad3(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska"))
                .filter(e -> e.getType().equals(ContractType.F))
                .forEach(System.out::println);

    }

    private static void zad2(List<Employee> employees) {
        employees.stream()
                .filter(e -> (e.getAge() % 2) == 0)
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }

    private static void zad1(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getSalary() >= 2500)
                .filter(e -> e.getSalary() < 3199)
                .forEach(System.out::println);
    }


}
