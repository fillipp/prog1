package hospital;

import java.util.Comparator;

public class Patient {

    private String name;
    private String surname;
    private int howAngry;
    private Disease disease;

    public String getName() {
        return name;
    }

    public void setName(String firstName) {
        this.name = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String lastName) {
        this.surname = lastName;
    }

    public int getHowAngry() {
        return howAngry;
    }

    public void setHowAngry(int howAngry) {
        this.howAngry = howAngry;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

}
