package hospital;

import java.util.PriorityQueue;

public class HospitalQueueService {
    private PriorityQueue<Patient> hospitalQueue
            = new PriorityQueue<>(new PatientComparator());


    public void addPatient(Patient patient) {
        hospitalQueue.add(patient);

    }

    public Patient handlePatient(){
        if (hospitalQueue.size() == 0) {
            System.out.println("Brak pacjentów\n");
            return  null;
        }
        return hospitalQueue.poll();
    }

    public Patient nextPatient(){
        return  hospitalQueue.peek();
    }

    public int queueSize(){
        return hospitalQueue.size();
    }
}
