package hospital;

import java.util.Scanner;
import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w rejstracji \n");

        String option;
        HospitalQueueService hospitalQueueService
                = new HospitalQueueService();
        do {
            printMenu();
            option = scanner.nextLine();
            if ("a".equals(option)) {
                System.out.println("Rejstracja nowego pacjenta");
                Patient newPatient = handleNewPatient(scanner);
                hospitalQueueService.addPatient(newPatient);
            } else if ("b".equals(option)) {
                Patient handledPatient =
                        hospitalQueueService.handlePatient();
                if (handledPatient != null){
                printPatientInfo(handledPatient);
                }
            } else if ("c".equals(option)) {

            } else if ("d".equals(option)) {

            }
        } while (!"q".equals(option));

    }

    private static void printMenu() {
        System.out.println(
                "a. Rejstracja nowego pacjenta \n" +
                        "b. Obsłuż pacjetna \n" +
                        "c. Ilość pacjetów w kolejce \n" +
                        "d. Następny pacjent \n" +
                        "q. Wyjscie \n"
        );
    }

    private static void printPatientInfo(Patient handledPatient) {
        System.out.println(
                new StringBuilder()
                        .append("Pacjent ")
                        .append(handledPatient.getName())
                        .append(" ")
                        .append(handledPatient.getSurname())
                        .append(" został przyjęty\n")
                        .toString()
        );
    }

    private static Patient handleNewPatient(Scanner scanner) {
        System.out.println("Imie: ");
        String name = scanner.nextLine();
        System.out.println("Nazwisko: ");
        String surname = scanner.nextLine();
        System.out.println("Złość: ");
        Integer howAngry = Integer.valueOf(scanner.nextInt());
        System.out.println("Choroba: ");
        scanner.nextLine();
        String diseaseStringVal2 = scanner.nextLine().toUpperCase();
        Disease disease = Disease.valueOf(diseaseStringVal2);

        Patient patient = new Patient();
        patient.setName(name);
        patient.setSurname(surname);
        patient.setHowAngry(howAngry);
        patient.setDisease(disease);
        System.out.println("Zarejstrowano pacjeta: " + name + " " + surname + "\n");
        return patient;
    }
}
