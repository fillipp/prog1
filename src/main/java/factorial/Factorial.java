package factorial;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wartość n dla której policzę silnie:");
        System.out.print("n = ");
        int n = scanner.nextInt();

        System.out.println(factorial(n));
    }

    static long factorial(long n) {
        if (n >= 1) {
            return n * factorial(n - 1);
        } else {
            return 1;
        }
    }


}

