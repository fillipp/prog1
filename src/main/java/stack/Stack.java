package stack;

public class Stack {
    private int array[];
    private int top;
    private int capacity;

    //Stack stack = new Stack(10)


    public Stack(int capacity) {
        this.capacity = capacity;
        this.array = new int[capacity];
        this.top = -1;
    }

    public void push(int element) {
        if (isFull()) {
            System.out.println("Stack is full");
        } else {
            array[++top] = element;
        }
    }

    private boolean isFull() {
        return false;
    }

    public int pop() {
        if (isEmpty()) {
            System.out.println("Stack is empty");
        } else {
            return array[top--];
        }
        return 0;
    }

    private boolean isEmpty() {
        return false;
    }

    public int peek() {
        return array[top];
    }


}
