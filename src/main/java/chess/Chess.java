package chess;

import java.util.Scanner;

public class Chess {

    public static final String BLAC_FIELD = " \u25A0 ";
    public static final String WHITE_FIELD = " \u25A1 ";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj rozmiar szachownicy: ");

        int chess_length = sc.nextInt();
        int w = chess_length;
        System.out.println("Rozmiar szachownicy: " + chess_length + "x" + chess_length);

        for (int y = 0; y < chess_length; y++) {
            for (int x = 0; x < chess_length; x++) {
                if ((x + y) % 2 == 0) {
                    System.out.print(BLAC_FIELD);
                } else {
                    System.out.print(WHITE_FIELD);
                }
            }

            System.out.println();

        }
    }
}
